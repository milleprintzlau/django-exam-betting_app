from rest_framework import generics, permissions
from rest_framework.response import Response
from .models import Bet
from .serializers import BetSerializer
from .permissions import IsOwnerOrNoAccess


class AllMyBets(generics.ListCreateAPIView):
   queryset = Bet.objects.all()
   serializer_class = BetSerializer

   def get_queryset(self):
      queryset = Bet.objects.filter(user=self.request.user)
      return queryset


class MyBet(generics.RetrieveUpdateDestroyAPIView):
   queryset = Bet.objects.all()
   serializer_class = BetSerializer