from rest_framework import serializers
from .models import Bet


class BetSerializer(serializers.ModelSerializer):
   class Meta:
      fields = ('id', 'user', 'game', 'selected_team', 'bet_amount')
      model = Bet