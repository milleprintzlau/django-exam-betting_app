from django.core.mail import send_mail


def email_message(message_dict):
   contents = f"""
   Hi, we have now transferred {message_dict['money']}
   to your bank account.
   """
   send_mail(
      'Your payout',
      contents,
      'ceciliatoll@hotmail.com',
      [message_dict['email_receiver']],
      fail_silently=False
   )