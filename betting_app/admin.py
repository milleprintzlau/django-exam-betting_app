from django.contrib import admin
from .models import UserProfile
from .models import UserBankDetail
from .models import Account
from .models import Ledger
from .models import Bet
from .models import Game
from .models import Winner

admin.site.register(UserProfile)
admin.site.register(UserBankDetail)
admin.site.register(Account)
admin.site.register(Ledger)
admin.site.register(Bet)
admin.site.register(Game)
admin.site.register(Winner)